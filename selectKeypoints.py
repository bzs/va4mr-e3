import numpy as np
import logging


def selectKeypoints(scores, num, r):
    """
    Selects the num best scores as keypoints and performs non-maximum supression of a (2r + 1)*(2r + 1) box around
    the current maximum.
    :return indices of keypoints, sorted by best to worst score
    """
    total_pix = np.prod(scores.shape)
    row, col = scores.shape

    def ind2sub(idx):
        rr = idx // col
        cc = idx - col * rr
        return rr, cc

    pivot = total_pix - num
    scores_view = scores.flatten()
    ind = np.argpartition(scores_view, pivot)
    thresh = scores_view[ind[pivot]]
    logging.debug(f"{thresh=}")
    keep_mask = scores > thresh
    top_points = ind[pivot:]
    for i, idx in enumerate(top_points):
        rr, cc = ind2sub(idx)
        score = scores[rr, cc]
        xmin = max(0, rr-r)
        xmax = min(row, rr+r)
        ymin = max(0, cc-r)
        ymax = min(col, cc+r)
        logging.debug(f"{i=} {score=} ({(rr, cc)}), selecting tl {(xmin)}, bl{xmax}, tr {ymin}, bt {ymax}")
        patch = scores[xmin:xmax, ymin:ymax]
        # commenting out the next line has the effect of not doing nms
        # visually examining the plot w/ no nms, it looks like the 200 keypoints
        # are highly clustered, so it looks like nms is doing the right thing
        # but still not sure about the clustering
        keep_mask[xmin:xmax, ymin:ymax] &= patch >= score
    logging.info(f" picking {np.sum(keep_mask)} keypoints out of {total_pix}")
    keypts_indices = np.array(np.nonzero(keep_mask)).T
    best_keypts_indices = sorted(keypts_indices, key=lambda x: scores[x[0], x[1]], reverse=True)
    best_keypts_indices = np.array(best_keypts_indices).T
    return best_keypts_indices

