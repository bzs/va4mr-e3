import numpy as np
import scipy.signal
from scipy import signal
from filters import sobel_x, sobel_y

import logging
logger = logging.getLogger(__name__)

def make_M_mat(img, patch_size):
    # output should be the same size as img
    # subimg = img[patch_radius:img_x-patch_radius, patch_radius:img_y-patch_radius]
    Ix = scipy.signal.convolve2d(img, sobel_x, 'valid')
    Iy = scipy.signal.convolve2d(img, sobel_y, 'valid')
    sum_kernel = np.ones((patch_size, patch_size))
    Ix2 = Ix ** 2
    Iy2 = Iy ** 2
    IxIy = Ix * Iy
    M11 = scipy.signal.convolve2d(Ix2, sum_kernel, 'valid')
    M12 = M21 = scipy.signal.convolve2d(IxIy, sum_kernel, 'valid')
    M22 = scipy.signal.convolve2d(Iy2, sum_kernel, 'valid')
    logger.debug(f"{Ix.shape=}")
    logger.debug(f"{M11.shape=}")
    num_reduced_pix = np.prod(M11.shape)
    M = np.zeros((num_reduced_pix, 2, 2))  # 2 by 3 by 2
    logger.debug(f"{M.shape=}")
    M[:, 0, 0] = M11.flatten()
    M[:, 0, 1] = M12.flatten()
    M[:, 1, 0] = M21.flatten()
    M[:, 1, 1] = M22.flatten()
    return M, M11.shape


def shi_tomasi(img, patch_size):
    logger.debug(f"{img.shape=}")
    M, _ = make_M_mat(img, patch_size)
    patch_radius = (patch_size-1)//2
    eigvals, _ = np.linalg.eig(M)
    logger.debug(f"{eigvals.shape=}")
    offset = 1 + patch_size
    score_shape = np.array(img.shape) - offset # because of sobel filter
    score = np.min(eigvals, 1).reshape(score_shape)
    score[score < 0] = 0
    logger.debug(f"{score.shape=}")
    o = offset // 2
    ret = np.pad(score, o, constant_values=0)
    return ret

