import numpy as np
from scipy import signal
from shi_tomasi import make_M_mat
import logging


def harris(img, patch_size, kappa):
    row, col = img.shape
    M, original_shape = make_M_mat(img, patch_size)
    A = M[:, 0, 0].flatten()
    B = M[:, 0, 1].flatten()
    C = M[:, 1, 0].flatten()
    D = M[:, 1, 1].flatten()
    dets = A*D - B*C
    traces = A+D
    logging.debug(f"{traces.shape=}")
    logging.debug(f"{dets.shape=}")
    score = dets - kappa * (traces**2)
    offset = 1 + patch_size
    score_shape = np.array(img.shape) - offset
    assert tuple(score_shape) == original_shape
    score = score.reshape(original_shape)
    score[score < 0] = 0
    # pad
    o = offset // 2
    out = np.pad(score, o, constant_values=0)
    assert out.shape == img.shape
    return out

