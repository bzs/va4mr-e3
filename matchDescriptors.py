import numpy as np
from scipy.spatial.distance import cdist
import logging


def matchDescriptors(query_descriptors, database_descriptors, match_lambda):
    """
    Returns a 1xQ matrix where the i-th coefficient is the index of the database descriptor which matches to the
    i-th query descriptor. The descriptor vectors are MxQ and MxD where M is the descriptor dimension and Q and D the
    amount of query and database descriptors respectively. matches(i) will be zero if there is no database descriptor
    with an SSD < lambda * min(SSD). No two non-zero elements of matches will be equal.
    """
    num_query = query_descriptors.shape[-1]
    num_db = database_descriptors.shape[-1]
    logging.debug(f"{num_query=}, {num_db=}")
    match_scores = cdist(query_descriptors.T, database_descriptors.T)
    out = np.argmin(match_scores, axis=1)
    min_ssd = np.min(match_scores)
    ret = out + 1
    for i, idx in enumerate(out):
        if match_scores[i, idx] >= min_ssd * match_lambda:
            ret[i] = 0
    return ret

