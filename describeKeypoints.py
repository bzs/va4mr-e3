import numpy as np
import logging


def describeKeypoints(img, keypoints, r):
    """
    Returns a (2r+1)^2xN matrix of image patch vectors based on image img and a 2xN matrix containing the keypoint
    coordinates. r is the patch "radius".
    """
    padded_img = np.pad(img, r, "edge")
    num_keypts = keypoints.shape[-1]
    out = np.zeros(((2*r+1)**2, num_keypts), dtype=img.dtype)
    logging.info(f"{out.shape=}")
    for idx, (x, y) in enumerate(keypoints.T):
        x += r  # due to padding
        y += r  # due to padding
        logging.debug(f"{(x, y)}")
        keypt = padded_img[x-r:x+r+1, y-r:y+r+1].flatten()
        logging.debug(f"{keypt.shape=}")
        out[:, idx] = keypt
    return out




